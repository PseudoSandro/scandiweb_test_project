<?php
namespace furniture
{
    //This class is made for handling the particular product type, excluding DB interaction
    class FurnitureService
    {
        public $type;
        public $sku;
        public $name;
        public $price;
        public $height;
        public $width;
        public $length;
        public $db_adress = "../db_manipulation/furniture_db.php";

        //Retrieves info from the POST superglobal
        public function __construct()
        {
            $this->type = $_POST["types"];
            $this->sku = $_POST["sku"];
            $this->name = $_POST["name"];
            $this->price = $_POST["price"];
            $this->height = $_POST["height"];
            $this->width = $_POST["width"];
            $this->length = $_POST["length"];
        }

        //This displays the product info on the "Form Control Page"
        public function displayAll()
        {
            echo "<div class='validationDisplay'>";
            echo "<p><strong>Type</strong>: $this->type</p>";
            echo "<p><strong>SKU</strong>: $this->sku</p>";
            echo "<p><strong>Name</strong>: $this->name</p>";
            echo "<p><strong>Price</strong>: $this->price</p>";
            echo "<p><strong>Height</strong>: $this->height MM</p>";
            echo "<p><strong>Width</strong>: $this->width MM</p>";
            echo "<p><strong>Length</strong>: $this->length MM</p>";
            echo "</div>";
        }

        //Displays Products on the "Product List" Page
        //Also, when generating html, i add the product TYPE and its SKU
        //To the checkbox VALUE field, it makes deletion possible and also the name of delete[], so that
        //this info is stored as an array
        public static function displayList($list)
        {
            echo "<h4>Furniture</h4>";
            echo "<div class='row'>";
            foreach ($list as $furniture) {
                $sku = $furniture['sku'];
                echo "<div class='item furniture col-lg-2 col-sm-4' id=$sku>";
                echo "<input id='check' type='checkbox' name='delete[]' value='FURNITURE#####$sku'>";
                echo "<em>" . $furniture['sku'] . "</em>" . "<br \>";
                echo "<strong>" .$furniture['name'] . "</strong>" ."<br \>";
                echo $furniture['price'] . "$" ."<br \>";
                echo $furniture['height'] . "x" . $furniture["width"] . "x" . $furniture["length"] .  "<br \>";
                echo "</div>";
            }
            echo "</div>";
        }

        //Formatting and Special attribute validation on the "Form control" page using RegEx
        public function validate()
        {
            $this->type = $this->trimmer($this->type);
            $this->sku = $this->trimmer($this->sku);
            $this->name = $this->trimmer($this->name);
            $this->price = $this->trimmer($this->price);
            $this->height = $this->trimmer($this->height);
            $this->width = $this->trimmer($this->width);
            $this->length = $this->trimmer($this->length);

            $height_matches = preg_match_all('/\d+/i', $this->height);
            $width_matches = preg_match_all('/\d+/i', $this->width);
            $length_matches = preg_match_all('/\d+/i', $this->length);

            if (!$height_matches) {
                echo "<p id='error'>ERROR: HEIGHT is not in the correct format</p>";
            }

            if (!$width_matches) {
                echo '<p id="error">ERROR: WIDTH is not in the correct format</p>';
            }

            if (!$length_matches) {
                echo '<p id="error">ERROR: LENGTH is not in the correct format</p>';
            }
        }

        //Basic formatting - removing whitespace, slashes and converting it to htmlspecialchars
        public function trimmer($data)
        {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }

        //Function for assigning variables to the SESSION superglobal
        public function sessionSet($sesh)
        {
            $sesh["type"] = $this->type;
            $sesh["sku"] = $this->sku;
            $sesh["name"] = $this->name;
            $sesh["price"] = $this->price;
            $sesh["height"] = $this->height;
            $sesh["width"] = $this->width;
            $sesh["length"] = $this->length;
            return $sesh;
        }
    }
}
