<?php
namespace books
{
    //This class is made for handling the particular product type, excluding DB interaction
    class BookService
    {
        public $type;
        public $sku;
        public $name;
        public $price;
        public $weight;
        public $db_adress = "../db_manipulation/books_db.php";

        //Retrieves info from the POST superglobal
        public function __construct()
        {
            $this->type = $_POST["types"];
            $this->sku = $_POST["sku"];
            $this->name = $_POST["name"];
            $this->price = $_POST["price"];
            $this->weight = $_POST["weight"];
        }

        //This displays the product info on the "Form Control Page"
        public function displayAll()
        {
            echo "<div class='validationDisplay'>";
            echo "<p><strong>Type</strong>: $this->type</p>";
            echo "<p><strong>SKU</strong>: $this->sku</p>";
            echo "<p><strong>Name</strong>: $this->name</p>";
            echo "<p><strong>Price</strong>: $this->price $</p>";
            echo "<p><strong>Weight</strong>: $this->weight KG</p>";
            echo "</div>";
        }

        //Displays Products on the "Product List" Page
        //Also, when generating html, i add the product TYPE and its SKU
        //To the checkbox VALUE field, it makes deletion possible, and also the name of delete[], so that
        //this info is stored as an array
        public static function displayList($list)
        {
            echo "<h4>Books</h4>";
            echo "<div class='row'>";
            foreach ($list as $book) {
                $sku = $book['sku'];
                echo "<div class='item book col-lg-2 col-sm-4' id=$sku>";
                echo "<input id='check' type='checkbox' name='delete[]' value='BOOKS#####$sku'>";
                echo "<em>" . $book['sku'] . "</em>" . "<br \>";
                echo "<strong>" .$book['name'] . "</strong>" ."<br \>";
                echo $book['price'] . "$" ."<br \>";
                echo $book['weight'] . " KG" . "<br \>";
                echo "</div>";
            }
            echo "</div>";
        }

        //Formatting and Special attribute validation on the "Form control" page using RegEx
        public function validate()
        {
            $this->type = $this->trimmer($this->type);
            $this->sku = $this->trimmer($this->sku);
            $this->name = $this->trimmer($this->name);
            $this->price = $this->trimmer($this->price);
            $this->weight = $this->trimmer($this->weight);

            $weight_matches = preg_match_all('/\d+/i', $this->weight);

            if (!$weight_matches) {
                echo "<p id='error'>ERROR: WEIGHT is not in the correct format</p>";
            }
        }

        //Basic formatting - removing whitespace, slashes and converting it to htmlspecialchars
        public function trimmer($data)
        {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }

        //Function for assigning variables to the SESSION superglobal
        public function sessionSet($sesh)
        {
            $sesh["type"] = $this->type;
            $sesh["sku"] = $this->sku;
            $sesh["name"] = $this->name;
            $sesh["price"] = $this->price;
            $sesh["weight"] = $this->weight;
            return $sesh;
        }
    }
}
