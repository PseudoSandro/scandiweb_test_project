<?php
namespace dvd
{
    //This class is made for handling the particular product type, excluding DB interaction
    class DvdService
    {
        public $type;
        public $sku;
        public $name;
        public $price;
        public $size;
        public $db_adress = "../db_manipulation/dvd_db.php";
        
        //Retrieves info from the POST superglobal
        public function __construct()
        {
            $this->type = $_POST["types"];
            $this->sku = $_POST["sku"];
            $this->name = $_POST["name"];
            $this->price = $_POST["price"];
            $this->size = $_POST["size"];
        }

        //This displays the product info on the "Form Control Page"
        public function displayAll()
        {
            echo "<div class='validationDisplay'>";
            echo "<p><strong>Type</strong>: $this->type</p>";
            echo "<p><strong>SKU</strong>: $this->sku</p>";
            echo "<p><strong>Name</strong>: $this->name</p>";
            echo "<p><strong>Price</strong>: $this->price $</p>";
            echo "<p><strong>Size</strong>: $this->size MB</p>";
            echo "</div>";
        }

        //Displays Products on the "Product List" Page
        //Also, when generating html, i add the product TYPE and its SKU
        //To the checkbox VALUE field, it makes deletion possible and also the name of delete[], so that
        //this info is stored as an array
        public static function displayList($list)
        {
            echo "<h4>DVDs</h4>";
            echo "<div class='row'>";
            foreach ($list as $dvd) {
                $sku = $dvd['sku'];
                echo "<div class='item dvd col-lg-2 col-sm-4' id=$sku>";
                echo "<input id='check' type='checkbox' name='delete[]' value='DVDS#####$sku'>";
                echo "<em>" . $dvd['sku'] . "</em>" ."<br \>";
                echo "<strong>" .$dvd['name'] . "</strong>" ."<br \>";
                echo $dvd['price'] . "$" . "<br \>";
                echo $dvd['size'] . " MB" . "<br \>";
                echo "</div>";
            }
            echo "</div>";
        }
        
        //Formatting and Special attribute validation on the "Form control" page using RegEx
        public function validate()
        {
            $this->type = $this->trimmer($this->type);
            $this->sku = $this->trimmer($this->sku);
            $this->name = $this->trimmer($this->name);
            $this->price = $this->trimmer($this->price);
            $this->size = $this->trimmer($this->size);

            $price_matches = preg_match_all('/^\d+.\d\d$/i', $this->price);
            $size_matches = preg_match_all('/\d+/i', $this->size);

            if (!$price_matches) {
                echo "<p id='error'>ERROR: PRICE is not in the correct format</p>";
            }

            if (!$size_matches) {
                echo '<p id="error">ERROR: SIZE is not in the correct format</p>';
            }
        }

        //Basic formatting - removing whitespace, slashes and converting it to htmlspecialchars
        public function trimmer($data)
        {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }

        //Function for assigning variables to the SESSION superglobal
        public function sessionSet($sesh)
        {
            $sesh["type"] = $this->type;
            $sesh["sku"] = $this->sku;
            $sesh["name"] = $this->name;
            $sesh["price"] = $this->price;
            $sesh["size"] = $this->size;
            return $sesh;
        }
    }
}
