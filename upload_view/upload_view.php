<?php
    //This Page displays to the user the product he/she is trying to upload


    //Starts the PHP session, we will use it to track data between pages
    session_start();

    //Based on the product type, we choose which file to use for handling it
switch ($_POST["types"]) {
    case "dvd":
        require_once("../product_classes/dvd_service.php");
        $service = new dvd\DvdService();
        break;
    case "book":
        require_once("../product_classes/book_service.php");
        $service = new books\BookService();
        break;
    case "furniture":
        require_once("../product_classes/furniture_service.php");
        $service = new furniture\FurnitureService();
        break;
};
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Upload View</title>
    <!--BOOTSTRAP CSS-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <!--JQUERY-->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" 
    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
    crossorigin="anonymous"></script>
    <!--BOOTSTRAP JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>

<body>
    <div class="container-fluid">
        <div class="d-flex flex-column align-items-center text-left">
            <h3>This is your product</h3>
            <?php
                //This prints error messages to the user, if the info is not validated correctly
                $service->validate();
                //Displays the info
                $service->displayAll();
                $_SESSION = $service->sessionSet($_SESSION);
            ?>
            <a href=<?php echo $service->db_adress?>>
            <button class="btn btn-primary">SAVE</button>
            </a>
            <br />
            <a href="../index.php">
                <button class="btn btn-info">Back To List View</button></a>
        </div>
    </div>
</body>
</html>