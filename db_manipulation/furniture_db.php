<?php
//This page is a "Landing page" for uploading product info to the db for the particular product type
session_start();
//The user is redirected to the "Product List" page after 2 seconds
header("refresh:2;url='../index.php'");
include_once("db_connection.php");

//Checks the connection to the database, Creates the relevant Table if none exists
$connection = new DbConnect();
$pdo = $connection->connect();
$exists = $connection->checkTable('FURNITURE');

if ($exists === "does not exist") {
    $pdo->query("CREATE TABLE FURNITURE (
        sku varchar(100) PRIMARY KEY NOT NULL,
        name varchar(100) NOT NULL,
        price DECIMAL(10, 2) NOT NULL,
        height INT NOT NULL,
        width INT NOT NULL,
        length INT NOT NULL
    )");
}

//Grabs the relevant information from the session variable for the given product type
$sku = strval($_SESSION['sku']);
$name = strval($_SESSION['name']);
$price = floatval($_SESSION['price']);
$height = intval($_SESSION['height']);
$width = intval($_SESSION['width']);
$length = intval($_SESSION['length']);

$sql = "INSERT INTO FURNITURE (sku, name, price, height, width, length) 
        VALUES('$sku', '$name', $price, $height, $width, $length)";
$pdo->exec($sql);

//The user sees that the product is being added
echo "Adding your Product...";

session_unset();
session_destroy();
