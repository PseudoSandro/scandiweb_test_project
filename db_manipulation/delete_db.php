<?php
// Product deletion handling page

header("refresh:2;url='../index.php'");

//Sets up PDO
include_once("db_connection.php");
$connection = new DbConnect();
$pdo = $connection->connect();

//Grabs the products to be deleted from _POST and pushes them to an array
//in the product classes, i inserted ##### between the TABLE name and the
//SKU of the product, so that i could extract them separately here using the
//explode function
$delete_array = [];
$count = count($_POST['delete']);
for ($i = 0; $i < $count; $i++) {
    $db_arr = explode("#####", $_POST['delete'][$i]);
    array_push($delete_array, $db_arr);
}

//Executes DELETE command for every member of the deletion array, based
//on the TABLE they are in and the SKU they have
$delete_count = count($delete_array);
for ($i = 0; $i < $delete_count; $i++) {
    $table = strval($delete_array[$i][0]);
    $sku = strval($delete_array[$i][1]);

    $sql = "DELETE FROM $table WHERE sku = '$sku'";
    $pdo->exec($sql);
}

echo "Deleting your products...";
