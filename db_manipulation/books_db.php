<?php
//This page is a "Landing page" for uploading product info to the db for the particular product type
session_start();
//The user is redirected to the "Product List" page after 2 seconds
header("refresh:2;url='../index.php'");
include_once("db_connection.php");

//Checks the connection to the database, Creates the relevant Table if none exists
$connection = new DbConnect();
$pdo = $connection->connect();
$exists = $connection->checkTable('BOOKS');

if ($exists === "does not exist") {
    $pdo->query("CREATE TABLE BOOKS (
        sku varchar(100) PRIMARY KEY NOT NULL,
        name varchar(100) NOT NULL,
        price DECIMAL(10, 2) NOT NULL,
        weight INT NOT NULL
    )");
}

//Grabs the relevant information from the session variable for the given product type
$sku = strval($_SESSION['sku']);
$name = strval($_SESSION['name']);
$price = floatval($_SESSION['price']);
$weight = intval($_SESSION['weight']);

$sql = "INSERT INTO BOOKS (sku, name, price, weight)
        VALUES('$sku', '$name', $price, $weight)";
$pdo->exec($sql);

//The user sees that the product is being added
echo "Adding your Product...";

session_unset();
session_destroy();
