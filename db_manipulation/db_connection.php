<?php
//Class for connecting to the database, also checking whether a given database exists
//This class does not have a namespace, because for some reason the PDO class (MYSQLi also, i tried it to no avail)
//Was not accessible when under a namespace
class DbConnect
{
    public $pdo;

    //Function that creates the database 'products' if it is not already created
    public function createDB()
    {
        $servername = 'localhost';
        $username = "root";
        $dbname = "products";

        try {
            $creator = new PDO("mysql:host=$servername", $username);
            $sql = "CREATE DATABASE $dbname";
            $creator->exec($sql);
        } catch (PDOException $e) {
            $e->getMessage();
        }
    }

    //function for connecting to DB using PDO
    public function connect()
    {
        $servername = 'localhost';
        $username = "root";
        $dbname = "products";
        try {
            $this->pdo = new PDO("mysql:host=$servername;dbname=$dbname", $username);
            return $this->pdo;
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    //This function checks if there is already a table with the given name in the DB
    public function checkTable($table_name)
    {
        try {
            $result = $this->pdo->query("SELECT 1 FROM $table_name LIMIT 1");
            $exists = "exists";
            return $exists;
        } catch (Exception $e) {
            $exists = "does not exist";
            return $exists;
        }
    }
}
