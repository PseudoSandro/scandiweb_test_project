<?php
//The main page that displays Products that we have saved to the Database

include_once("db_manipulation/db_connection.php");
include_once("product_classes/dvd_service.php");
include_once("product_classes/book_service.php");
include_once("product_classes/furniture_service.php");

//Initializes variables for holding products from the given category
$DVDlist = null;
$FurnitureList = null;
$Booklist = null;

$connection = new DbConnect();

//Creates the database "Products" if it already does not exist
$connection->createDB();

//Initializec PDO connection
$pdo = $connection->connect();

//The database is divided in 3 Tables - one for each product type
//This checks whether the tables exist, and if they do exist, fetches products that are in those tables
$DVDExists = $connection->checkTable("DVDS");
$FurnitureExists = $connection->checkTable("FURNITURE");
$BookExists = $connection->checkTable("BOOKS");

if ($DVDExists === "exists") {
    $DVDlist = $pdo->query("SELECT * FROM DVDS")->fetchAll();
}

if ($FurnitureExists === "exists") {
    $FurnitureList = $pdo->query("SELECT * FROM FURNITURE")->fetchAll();
}

if ($BookExists === "exists") {
    $Booklist = $pdo->query("SELECT * FROM BOOKS")->fetchAll();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product List</title>
    <script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <style>
        <?php include 'index.css'; ?>
    </style>
</head>
<body>
    <div class="container-fluid">
        <header id="headerContainer">
            <h1>Product List</h1>
            <nav id="addButton">
                <a href="product_add/product_add.html">
                    <button class="btn btn-primary">Add New</button>
                </a>
                <button class ="btn btn-danger" type="submit" form="deleteForm" disabled=true>DELETE SELECTED</button>
            </nav>
        </header>
        <hr />
        <!-- The form tag allows the checkboxes to be posted to the deletion page-->
        <form method="post" action="db_manipulation/delete_db.php" id="deleteForm">
    <?php
    //The following statements display fetched data, if corresponding tables exist in the DB and they
    //have a record in them
    if ($DVDlist !== null) {
        if (count($DVDlist) > 0) {
            dvd\DvdService::displayList($DVDlist);
        }
    }
    if ($FurnitureList !== null) {
        if (count($FurnitureList) > 0) {
            furniture\FurnitureService::displayLIst($FurnitureList);
        }
    }

    if ($Booklist !== null) {
        if (count($Booklist) > 0) {
            books\BookService::displayList($Booklist);
        }
    }
    ?>
    </div>
    <script>
        //This Script Enables and Disables the DELETE button, depending on whether at least
        //One checkbox is checked
        $("input").click(() => {
            if($("input:checked").length == 0) {
                $(".btn-danger").attr("disabled", true)
                return;
            }
            $(".btn-danger").attr("disabled", false)
        });
    </script>
</body>
</form>
</html>